firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.

    $(".logged-out").hide();
    $(".logged-in").show();

    var user = firebase.auth().currentUser;

    if (user.emailVerified == false) {
    	$(".mail-verification").show();
    	$(".search-bar").hide();
    } else {
    	$(".mail-verification").hide();
    	$(".search-bar").show();
    }

  } else {
    // No user is signed in.

    $(".logged-out").show();
    $(".logged-in").hide();
    $(".search-bar").hide();
    $(".notLogMsg").show();

  }
});

function deconnexion(){
	firebase.auth().signOut();
}

function resendmail(){
	var user = firebase.auth().currentUser;

	user.sendEmailVerification().then(function() {
$("body").overhang({
  type: "success",
  message: "Mail envoyé",
  duration : 5
});
	}).catch(function(error) {
$("body").overhang({
  type: "error",
  message: "Probleme lors de l'envoie",
  duration : 5
});
	});
}

function reload(){
	window.location.reload();
}

mapboxgl.accessToken = 'pk.eyJ1IjoiZG9wZWVlZWVlZSIsImEiOiJja2F4MW5uazgwMXZwMndvMjhkMGdkdWxhIn0.oSODILd-DsUI40fYS6a9bQ';

var map = new mapboxgl.Map({
	container: 'map',
	style: 'mapbox://styles/mapbox/streets-v11',
	center: [3.066667, 50.633333],
	zoom: 8
});
 
var geocoder = new MapboxGeocoder({
	accessToken: mapboxgl.accessToken,
	marker: {
color: 'orange'
	},
	types: 'place',
	countries: 'fr',
	mapboxgl: mapboxgl
});

map.addControl(geocoder);

map.on('load', function(){
	geocoder.on('result', function(ev){
var styleSpec = ev.result;
var json = JSON.stringify(styleSpec, null, 2);
var resultParse = JSON.parse(json);
var placeText = resultParse.text.toLowerCase();
var placeName = resultParse.place_name;
var longitude = resultParse.center[0];
var latitude = resultParse.center[1];
$("#cityLng").val(longitude);
$("#cityLat").val(latitude);
$("#mapResult").val(placeText);
$("#mapResult").text("Vous avez choisis : " + placeName);
$("#mapResult").show();
$("#btnValidateVille").show();
	});
});

var geocoder2 = new MapboxGeocoder({
	accessToken: mapboxgl.accessToken,
	countries: 'fr'
});
 
geocoder2.addTo('#geocoder');

geocoder2.on('result', function(ev){
var styleSpec = ev.result;
var json = JSON.stringify(styleSpec, null, 2);
var resultParse = JSON.parse(json);
$("#lieuResult").val(resultParse.place_name);
	});

function searchVille(){

	var db = firebase.firestore();
	var user = firebase.auth().currentUser;

	var ville = $("#search-bar-ville").val().toLowerCase();
	var docRef = db.collection("villes").doc(ville);

	var titreVille = $("#villeTitre");

	docRef.get().then(function(doc) {
	    if (doc.exists) {
	        titreVille.text(doc.data().name.toUpperCase());
	        $("#search-result").hide();
	        $("#viewResult").show();

	        var map2 = new mapboxgl.Map({
container: 'viewMap',
style: 'mapbox://styles/mapbox/streets-v11',
center: [doc.data().longitude, doc.data().latitude],
zoom: 11
	});
	 
	var marker = new mapboxgl.Marker()
	.setLngLat([doc.data().longitude, doc.data().latitude])
	.addTo(map2);

	map2.resize();

	$(".evenmentsListe").text("");

	db.collection("villes").doc(ville).collection("evenements").get().then(function(querySnapshot) {
	    querySnapshot.forEach(function(doc) {
	        var resultAppend = "<h2>" + doc.data().name + "</h2><p>Lieu : " + doc.data().lieu + "</p><p>Date : " + doc.data().date + "</p><p>Description : " + doc.data().description;
	        $(".evenmentsListe").append(resultAppend);
	    });
	});

	    } else {
	        $("#search-result").show();
	        $("#viewResult").hide();
	    }
	}).catch(function(error) {
	    console.log("Error getting document:", error);
	});

	db.collection("users").doc(user.uid).collection("abonnements").doc(ville).get().then(function(doc) {
	    if (doc.exists) {
	        $("#subBtn").hide();
	    } else {
	        // doc.data() will be undefined in this case
	        console.log("No such document!");
	        $("#subBtn").show();
	    }
	}).catch(function(error) {
	    console.log("Error getting document:", error);
	});

}

function showFormCreateVille(){

	$(".search-bar").hide();
	$(".form-ville").show();
	map.resize();

}

function createVille(){

	var db = firebase.firestore();

	var ville = $("#mapResult").val();
	var long = $("#cityLng").val();
	var lat = $("#cityLat").val();

	db.collection("villes").doc(ville).set({
	    name: ville,
	    longitude: long,
	    latitude: lat
	})
	.then(function() {
	    console.log("Document successfully written!");
	    location.reload();
	})
	.catch(function(error) {
	    console.error("Error writing document: ", error);
	});

}

function showFormEvent(){

	$(".listEvents").hide();
	$("#eventCreate").hide();

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();
	 if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    } 

	today = yyyy+'-'+mm+'-'+dd;
	document.getElementById("dateEvent").setAttribute("min", today);

	$(".formEvent").show();

}

function createEventForm(){

	var db = firebase.firestore();

	var eventName = $("#nameEvent").val();
	var eventLieu = $("#lieuResult").val();
	var eventDate = $("#dateEvent").val();
	var eventDescr = $("#descrEvent").val();
	var ville = $("#search-bar-ville").val().toLowerCase();

	db.collection("villes").doc(ville).collection("evenements").doc(eventName).set({
	    name: eventName,
	    lieu: eventLieu,
	    date: eventDate,
	    description: eventDescr
	})
	.then(function() {
	    console.log("Document successfully written!");
	})
	.catch(function(error) {
	    console.error("Error writing document: ", error);
	});

	$(".listEvents").show();
	$("#eventCreate").show();
	$(".formEvent").hide();
	$("#search-bar-ville").val(ville);
	$("#search-button").click();

}


function subVille(){

	var db = firebase.firestore();

	var user = firebase.auth().currentUser;
	var ville = $("#villeTitre").text().toLowerCase();

	db.collection("users").doc(user.uid).collection("abonnements").doc(ville).set({
	    name: ville
	})
	.then(function() {
	    console.log("Document successfully written!");
	})
	.catch(function(error) {
	    console.error("Error writing document: ", error);
	});

	$("#search-bar-ville").val(ville);
	$("#search-button").click();

}