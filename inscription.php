<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="viewport-fit=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="dist/overhang.min.css" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="dist/overhang.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style_inscription.css">
	<title>Projet-S4</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
	<a class="navbar-brand" href="#">Projet-S4</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		    <li class="nav-item">
		    	<a class="nav-link" href="index.php">Accueil</a>
		    </li>
		</ul>
		<ul class="navbar-nav navbar-right">
			<li class="nav-item active">
				<a class="nav-link logged-out" href="inscription.php">Inscription <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link logged-out" href="connexion.php">Connexion</a>
			</li>
			<li class="nav-item">
				<a class="nav-link logged-in" href="index.php" onclick="deconnexion()">Deconnexion</a>
			</li>
		</ul>
	</div> 
	</nav>

	<h1>Inscription</h1>

	<form id="signup-form">
		<table>
			<tr>
				<th>
					<label>Adresse e-mail :</label>
				</th>
				<th>
					<input type="email" id="signup-email" required>
				</th>
			</tr>
			<tr>
				<th>
					<label>Mot de passe :</label>
				</th>
				<th>
					<input type="password" id="signup-password" required>
				</th>
			</tr>
			<tr>
				<th>
					<label>Mot de passe :</label>
				</th>
				<th>
					<input type="password" id="signup-confirm" required>
				</th>
			</tr>
			<tr>
				<td>
					<i>Minimum 8 caractéres</i>
				</td>
			</tr>
			<tr>
				<th></th>
				<th><button id="inscription">Inscription</button></th>
			</tr>
		</table>
	</form>

	<!-- The core Firebase JS SDK is always required and must be listed first -->
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-auth.js"></script>

	<!-- TODO: Add SDKs for Firebase products that you want to use
	     https://firebase.google.com/docs/web/setup#available-libraries -->
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-analytics.js"></script>

	<script>
	  // Your web app's Firebase configuration
	  var firebaseConfig = {
	    apiKey: "AIzaSyDjCcS99CPSxZbMRKufEJkkJFwToRVvQeI",
	    authDomain: "test-firebase-web-e5b98.firebaseapp.com",
	    databaseURL: "https://test-firebase-web-e5b98.firebaseio.com",
	    projectId: "test-firebase-web-e5b98",
	    storageBucket: "test-firebase-web-e5b98.appspot.com",
	    messagingSenderId: "800524179044",
	    appId: "1:800524179044:web:fe45fae91ec67b7d157171",
	    measurementId: "G-BKEPR0Z768"
	  };
	  // Initialize Firebase
	  firebase.initializeApp(firebaseConfig);
	  firebase.analytics();
	</script>

	<script type="text/javascript">

		const signupForm = document.querySelector('#signup-form');
		signupForm.addEventListener('submit', (e) => {
		  e.preventDefault();
		  
		  // recupere les infos
		  const email = signupForm['signup-email'].value;
		  const password = signupForm['signup-password'].value;
		  const confirm = signupForm['signup-confirm'].value;

		  const auth = firebase.auth();

		  if (password.length >= 8) {
		  	if (password == confirm) {
		  		// creer le compte
				  auth.createUserWithEmailAndPassword(email, password).then(cred => {

					var user = firebase.auth().currentUser;
					// envoie un mail de verification
					user.sendEmailVerification().then(function() {
					  	$("body").overhang({
						  type: "success",
						  message: "Votre compte a était crée, merci de vérifier votre mail, vous allez être redirigé",
						  duration : 5
						});
						setTimeout(function(){
				            window.location.href = 'connexion.php';
				         }, 8000);
					}).catch(function(error) {
						var errorCode = error.code;
					  	var errorMessage = error.message;
					  	console.log(errorCode);
					  	console.log(errorMessage);
					});
				  });
		  	} else {
		  		$("body").overhang({
				  type: "error",
				  message: "Les mots de passe ne correspondent pas",
				  duration : 5
				});
		  	}
		  } else {
		  	$("body").overhang({
			  type: "error",
			  message: "Mot de passe trop court",
			  duration : 5
			});
		  }
		});

	</script>

</body>
</html>