<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
  		$('a[rel*=facebox]').facebox()
	})
	
</script>
<link href="css/facebox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="js/facebox.js" type="text/javascript"></script>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="viewport-fit=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="dist/overhang.min.css" />
    <script type="text/javascript" src="dist/overhang.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script src='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.js'></script>
	<link href='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css' rel='stylesheet' />
    <link rel="stylesheet" href="css/style_index.css">
	<title>Projet-S4</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
	<a class="navbar-brand" href="#">Projet-S4</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		    <li class="nav-item active">
		    	<a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
		    </li>
		</ul>
		<ul class="navbar-nav navbar-right">
			<li class="nav-item">
				<a class="nav-link logged-out" href="inscription.php">Inscription</a>
			</li>
			<li class="nav-item">
				<a class="nav-link logged-out" href="connexion.php">Connexion</a>
			</li>
			<li class="nav-item">
				<a class="nav-link logged-in" href="index.php" onclick="deconnexion()">Deconnexion</a>
			</li>
		</ul>
	</div> 
	</nav>

	<div class="notLogMsg">
		<h3>Merci de vous connecter ou créer un compte pour utiliser notre site</h3>
	</div>

	<div class="mail-verification">
		<h2>Merci de verifier votre mail afin d'utiliser notre site</h2>
		<button id="btn-verification" onclick="resendmail()">Renvoyer le mail</button>
		<button id="btn-reload" onclick="reload()">Actualiser la page</button>
	</div>

	<div class="search-bar">
		<div id="inputSearch">
			<input class="form-control" id="search-bar-ville" type="text" placeholder="Rechercher une ville..." aria-label="Search" required>
	  		<button id="search-button" onclick="searchVille()">Rechercher</button>
		</div>
	  	<div id="search-result">
			<p>La ville que vous rechercher n'existe pas, voulez vous la créer ?</p>
			<button id="create-ville" onclick="showFormCreateVille()">Créer la ville</button>
		</div>
		<div id="viewResult">
			<h2 id="villeTitre"></h2>
			<div id="viewMap"></div>
			<button class="buttonsVille" id="subBtn" onclick="subVille()">S'abonner à la ville</button>
			<div class="listEvents">
				<h2 style="padding-top: 4%; padding-bottom: 4%;">SES EVENEMENTS</h2>
				<div class="evenmentsListe"></div>
			</div>
			<div class="formEvent">
				<table>
					<tr>
						<th>
							<label>Nom de l'événement :</label>
						</th>
						<th>
							<input type="text" id="nameEvent" required>
						</th>
					</tr>
					<tr>
						<th>
							<label>Lieu de l'événement :</label>
						</th>
						<th>
							<div id="geocoder"></div>
							<input type="hidden" id="lieuResult">
						</th>
					</tr>
					<tr>
						<th>
							<label>Date de l'événement :</label>
						</th>
						<th>
							<input type="date" id="dateEvent" required>
						</th>
					</tr>
					<tr>
						<th>
							<label>Description de l'événement :</label>
						</th>
						<th>
							<input type="text" id="descrEvent" required>
						</th>
					</tr>
					<tr>
						<th></th>
						<th><button id="creerEvent" onclick="createEventForm()">Créer l'événement</button></th>
					</tr>
				</table>
			</div>
			<button class="buttonsVille" id="eventCreate" onclick="showFormEvent()">Créer un événement</button>
		</div>
	</div>

	<div class="form-ville">

		<div class="mapSearch">
			<h3>Merci de saisir la ville à créer :</h3>
			<div id='map' style="width: 100%; height: 400px;"></div>
			<input type="hidden" id="cityLat" name="cityLat" />
			<input type="hidden" id="cityLng" name="cityLng" /> 
			<p id="mapResult" style="text-align: center; margin-top: 2%; display: none;"></p>
			<button id="btnValidateVille" onclick="createVille()">Créer cette ville</button>
		</div>

	</div>

	<!-- The core Firebase JS SDK is always required and must be listed first -->
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-auth.js"></script>
	<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-firestore.js"></script>

	<!-- TODO: Add SDKs for Firebase products that you want to use
	     https://firebase.google.com/docs/web/setup#available-libraries -->
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-analytics.js"></script>

	<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
	<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css" type="text/css"/>

	<script>
	  // Your web app's Firebase configuration
	  var firebaseConfig = {
	    apiKey: "AIzaSyDjCcS99CPSxZbMRKufEJkkJFwToRVvQeI",
	    authDomain: "test-firebase-web-e5b98.firebaseapp.com",
	    databaseURL: "https://test-firebase-web-e5b98.firebaseio.com",
	    projectId: "test-firebase-web-e5b98",
	    storageBucket: "test-firebase-web-e5b98.appspot.com",
	    messagingSenderId: "800524179044",
	    appId: "1:800524179044:web:fe45fae91ec67b7d157171",
	    measurementId: "G-BKEPR0Z768"
	  };
	  // Initialize Firebase
	  firebase.initializeApp(firebaseConfig);
	  firebase.analytics();
	</script>

	<script type="text/javascript" src="js/index.js"></script>

</body>
</html>