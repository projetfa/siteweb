<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="viewport-fit=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="dist/overhang.min.css" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="dist/overhang.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style_connexion.css">
	<title>Projet-S4</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
	<a class="navbar-brand" href="#">Projet-S4</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		    <li class="nav-item">
		    	<a class="nav-link" href="index.php">Accueil</a>
		    </li>
		</ul>
		<ul class="navbar-nav navbar-right">
			<li class="nav-item">
				<a class="nav-link logged-out" href="inscription.php">Inscription</a>
			</li>
			<li class="nav-item active">
				<a class="nav-link logged-out" href="connexion.php">Connexion <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link logged-in" href="index.php" onclick="deconnexion()">Deconnexion</a>
			</li>
		</ul>
	</div> 
	</nav>

	<h1>Connexion</h1>

	<div id="login-form">
		<table>
			<tr>
				<th>
					<label>Adresse e-mail :</label>
				</th>
				<th>
					<input type="email" id="login-email" required>
				</th>
			</tr>
			<tr>
				<th>
					<label>Mot de passe :</label>
				</th>
				<th>
					<input type="password" id="login-password" required>
				</th>
			</tr>
			<tr>
				<th></th>
				<th><button id="connexion" onclick="connexion()">Connexion</button></th>
			</tr>
		</table>
	</div>

	<!-- The core Firebase JS SDK is always required and must be listed first -->
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-auth.js"></script>

	<!-- TODO: Add SDKs for Firebase products that you want to use
	     https://firebase.google.com/docs/web/setup#available-libraries -->
	<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-analytics.js"></script>

	<script>
	  // Your web app's Firebase configuration
	  var firebaseConfig = {
	    apiKey: "AIzaSyDjCcS99CPSxZbMRKufEJkkJFwToRVvQeI",
	    authDomain: "test-firebase-web-e5b98.firebaseapp.com",
	    databaseURL: "https://test-firebase-web-e5b98.firebaseio.com",
	    projectId: "test-firebase-web-e5b98",
	    storageBucket: "test-firebase-web-e5b98.appspot.com",
	    messagingSenderId: "800524179044",
	    appId: "1:800524179044:web:fe45fae91ec67b7d157171",
	    measurementId: "G-BKEPR0Z768"
	  };
	  // Initialize Firebase
	  firebase.initializeApp(firebaseConfig);
	  firebase.analytics();
	</script>

	<script type="text/javascript">

		function connexion(){

			firebase.auth().languageCode = 'fr';

			//recupere les infos saisies
			var email = $("#login-email").val();
			var password = $("#login-password").val();

			firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
				window.location.href = 'index.php';
			}).catch(function(error) {
			  	var errorCode = error.code;
			  	var errorMessage = error.message;
			  	console.log(errorCode);
			  	console.log(errorMessage);
			});
		}

	</script>

</body>
</html>